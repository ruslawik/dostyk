<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MyStore extends Model
{
    use SoftDeletes;

    public $table = 'my_stores';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'book_id',
        'pack_id',
        'status_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'book_number_id',
    ];

    public function book()
    {
        return $this->belongsTo(Book::class, 'book_id');
    }

    public function pack()
    {
        return $this->belongsTo(BookPack::class, 'pack_id');
    }

    public function book_number()
    {
        return $this->belongsTo(Order::class, 'book_number_id');
    }

    public function status()
    {
        return $this->belongsTo(Order::class, 'status_id');
    }
}
