<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    public $table = 'orders';

    protected $dates = [
        'order_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'number',
        'status',
        'book_id',
        'pack_id',
        'store_id',
        'order_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const STATUS_SELECT = [
        '0' => 'На одобрении',
        '1' => 'Отправлено в типографию',
        '2' => 'Отправлено получателю',
        '3' => 'На складе',
    ];

    public function myStores()
    {
        return $this->hasMany(MyStore::class, 'book_number_id', 'id');
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }

    public function sended() {
        return $this->hasMany("App\Sended");
    }

    public function book()
    {
        return $this->belongsTo(Book::class, 'book_id');
    }

    public function pack()
    {
        return $this->belongsTo(BookPack::class, 'pack_id');
    }

    public function getOrderDateAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setOrderDateAttribute($value)
    {
        $this->attributes['order_date'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }
}
