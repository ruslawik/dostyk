<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookPack extends Model
{
    use SoftDeletes;

    public $table = 'book_packs';

    protected $dates = [
        'date_to',
        'date_from',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'comment',
        'date_to',
        'date_from',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function books()
    {
        return $this->hasMany(Book::class, 'pack_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'pack_id', 'id');
    }

    public function myStores()
    {
        return $this->hasMany(MyStore::class, 'pack_id', 'id');
    }

    public function getDateFromAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setDateFromAttribute($value)
    {
        $this->attributes['date_from'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function getDateToAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setDateToAttribute($value)
    {
        $this->attributes['date_to'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }
}
