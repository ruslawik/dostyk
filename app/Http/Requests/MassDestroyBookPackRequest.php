<?php

namespace App\Http\Requests;

use App\BookPack;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyBookPackRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('book_pack_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:book_packs,id',
        ];
    }
}
