<?php

namespace App\Http\Requests;

use App\MyStore;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreMyStoreRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('my_store_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
        ];
    }
}
