<?php

namespace App\Http\Requests;

use App\Order;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreOrderRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('order_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'store_id'   => [
                'required',
                'integer',
            ],
            'book_id'    => [
                'required',
                'integer',
            ],
            'number'     => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'order_date' => [
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
            ],
        ];
    }
}
