<?php

namespace App\Http\Controllers\Admin;

use App\Book;
use App\BookPack;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyBookRequest;
use App\Http\Requests\StoreBookRequest;
use App\Http\Requests\UpdateBookRequest;
use Gate;
use Auth;
use App\Order;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class BookController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if(Auth::user()->roles[0]['id']==1){
                $query = Book::with(['pack'])->select(sprintf('%s.*', (new Book)->table));
            }else{
                $query = Book::where("not_show", 0)->with(['pack'])->select(sprintf('%s.*', (new Book)->table));
            }
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'book_show';
                $editGate      = 'book_edit';
                $deleteGate    = 'book_delete';
                $crudRoutePart = 'books';
                $order         = 'order';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'order',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('book_name', function ($row) {
                return $row->book_name ? $row->book_name : "";
            });
            $table->editColumn('grade', function ($row) {
                return $row->grade ? Book::GRADE_SELECT[$row->grade] : '';
            });
            $table->editColumn('lang', function ($row) {
                return $row->lang ? Book::LANG_SELECT[$row->lang] : '';
            });
            $table->editColumn('price', function ($row) {
                return $row->price ? $row->price : "";
            });
            $table->addColumn('pack_name', function ($row) {
                return $row->pack ? $row->pack->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'pack']);

            return $table->make(true);
        }

        return view('admin.books.index');
    }

    public function create()
    {
        abort_if(Gate::denies('book_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $packs = BookPack::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.books.create', compact('packs'));
    }

    public function store(StoreBookRequest $request)
    {
        $book = Book::create($request->all());

        return redirect()->route('admin.books.index');
    }

    public function edit(Book $book)
    {
        abort_if(Gate::denies('book_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $packs = BookPack::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $book->load('pack');

        return view('admin.books.edit', compact('packs', 'book'));
    }

    public function update(UpdateBookRequest $request, Book $book)
    {
        $book->update($request->all());

        return redirect()->route('admin.books.index');
    }

    public function show(Book $book)
    {
        abort_if(Gate::denies('book_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $book->load('pack');

        return view('admin.books.show', compact('book'));
    }

    public function destroy(Book $book)
    {
        abort_if(Gate::denies('book_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $q = Order::where("book_id", $book['id'])->count();
        if($q > 0){
            return back()->withErrors(['Ошибка', 'Книга уже была заказана и не может быть удалена!']);
        }else{
            $book->delete();
            return back();
        }
    }

    public function massDestroy(MassDestroyBookRequest $request)
    {
        Book::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
