<?php

namespace App\Http\Controllers\Admin;

use App\BookPack;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyBookPackRequest;
use App\Http\Requests\StoreBookPackRequest;
use App\Http\Requests\UpdateBookPackRequest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class BookPacksController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $query = BookPack::query()->select(sprintf('%s.*', (new BookPack)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'book_pack_show';
                $editGate      = 'book_pack_edit';
                $deleteGate    = 'book_pack_delete';
                $crudRoutePart = 'book-packs';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->editColumn('comment', function ($row) {
                return $row->comment ? $row->comment : "";
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.bookPacks.index');
    }

    public function create()
    {
        abort_if(Gate::denies('book_pack_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.bookPacks.create');
    }

    public function store(StoreBookPackRequest $request)
    {
        $bookPack = BookPack::create($request->all());

        return redirect()->route('admin.book-packs.index');
    }

    public function edit(BookPack $bookPack)
    {
        abort_if(Gate::denies('book_pack_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.bookPacks.edit', compact('bookPack'));
    }

    public function update(UpdateBookPackRequest $request, BookPack $bookPack)
    {
        $bookPack->update($request->all());

        return redirect()->route('admin.book-packs.index');
    }

    public function show(BookPack $bookPack)
    {
        abort_if(Gate::denies('book_pack_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.bookPacks.show', compact('bookPack'));
    }

    public function destroy(BookPack $bookPack)
    {
        abort_if(Gate::denies('book_pack_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $bookPack->delete();

        return back();
    }

    public function massDestroy(MassDestroyBookPackRequest $request)
    {
        BookPack::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
