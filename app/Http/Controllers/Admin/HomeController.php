<?php

namespace App\Http\Controllers\Admin;
use Auth;

class HomeController
{
    public function index()
    {
        if(Auth::user()->roles[0]['id']==3){
        	return redirect("/admin/orders");
        }
        if(Auth::user()->roles[0]['id']==2){
        	return redirect("/admin/my-stores");
        }
        if(Auth::user()->roles[0]['id']==1){
        	return redirect("/admin/orders");
        }
    }
}
