<?php

namespace App\Http\Controllers\Admin;

use App\Book;
use Auth;
use App\BookPack;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyOrderRequest;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Order;
use App\Sended;
use App\Store;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class OrderController extends Controller
{   

    public function make_order(Request $r) {

        foreach ($r->all() as $key => $value) {
            $ex = explode("_", $key);
            $book_id = $ex[1];
            
            if(is_numeric($book_id) && $value>0){
                $data = Book::where('id', $book_id)->get();

                $order = new Order();
                $order->number = $value;
                $order->status = 0;
                $order->store_id = Auth::user()->store['id'];
                $order->book_id = $book_id;
                $order->pack_id = $data['0']['pack_id'];
                $order->save();
            }
        }

        return redirect()->route('admin.orders.index');
    }

    public function send_to_typo(Request $r){
        $order_id = $r->input("order_id");
        Order::where("id", $order_id)->update(['status'=>1]);
        return back();
    }

    public function send_to (Request $r){

        $data = Order::where("id", $r->input("order_id"))->get();
        $sum = Sended::where("order_id", $r->input("order_id"))->sum('amount');

        $left = ($data[0]['number'])-(($r->input("book_number")+$sum));
        
        if($sum == 0){
            Sended::insert(["order_id"=>$r->input("order_id"), "amount"=>$r->input("book_number"), "status" => "1"]);
        }

        if($sum > 0){
            Sended::where("order_id", $r->input("order_id"))
                    ->update(["amount"=>($sum+$r->input("book_number")), "status" => "1"]);
        }

        if($left <= 0){
            Order::where("id", $r->input("order_id"))->update(["status" => 2]);
        }

        return redirect()->route('admin.orders.index');
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            if(Auth::user()->roles[0]['id']==2){
                $query = Order::with(['store', 'book', 'pack'])
                        ->where("store_id", Auth::user()->store['id'])
                        ->select(sprintf('%s.*', (new Order)->table));
            }else if(Auth::user()->roles[0]['id']==3){
                $query = Order::with(['store', 'book', 'pack'])
                        ->where('status', 1)
                        ->orWhere('status', 2)
                        ->select(sprintf('%s.*', (new Order)->table));
            }else{
                $query = Order::with(['store', 'book', 'pack'])->select(sprintf('%s.*', (new Order)->table));
            }
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'order_show';
                $editGate      = 'order_edit';
                $deleteGate    = 'order_delete';
                $send          = 'send';
                $send_typo     = 'send_typo';
                $crudRoutePart = 'orders';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'send',
                    'send_typo',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('store_name', function ($row) {
                return $row->store ? $row->store->name : '';
            });

            $table->addColumn('book_book_name', function ($row) {
                return $row->book ? $row->book->book_name : '';
            });

            $table->addColumn('pack_name', function ($row) {
                return $row->pack ? $row->pack->name : '';
            });

            $table->editColumn('number', function ($row) {
                $sum = Sended::where("order_id", $row->id)->sum('amount');
                if($row->status!=0 && $row->status!=3){
                    return $row->number ? "Заказано: ".$row->number : "";
                }else if($row->status==0){
                    return $row->number ? "Заказано: ".$row->number : "";
                }else if($row->status==3){
                    return $row->number ? "Заказано: ".$row->number : "";
                }
            });
            $table->editColumn('status', function ($row) {
                return Order::STATUS_SELECT[$row->status];
            });

            $table->rawColumns(['actions', 'placeholder', 'store', 'book', 'pack']);

            return $table->make(true);
        }

        return view('admin.orders.index');
    }

    public function create()
    {
        abort_if(Gate::denies('order_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $stores = Store::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $books = Book::all()->pluck('book_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $packs = BookPack::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.orders.create', compact('stores', 'books', 'packs'));
    }

    public function store(StoreOrderRequest $request)
    {
        $order = Order::create($request->all());

        return redirect()->route('admin.orders.index');
    }

    public function edit(Order $order)
    {
        abort_if(Gate::denies('order_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $stores = Store::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $books = Book::all()->pluck('book_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $packs = BookPack::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $order->load('store', 'book', 'pack');

        return view('admin.orders.edit', compact('stores', 'books', 'packs', 'order'));
    }

    public function update(UpdateOrderRequest $request, Order $order)
    {
        $order->update($request->all());

        return redirect()->route('admin.orders.index');
    }

    public function show(Order $order)
    {
        abort_if(Gate::denies('order_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $order->load('store', 'book', 'pack');

        return view('admin.orders.show', compact('order'));
    }

    public function destroy(Order $order)
    {
        abort_if(Gate::denies('order_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $order->delete();

        return back();
    }

    public function massDestroy(MassDestroyOrderRequest $request)
    {
        Order::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
