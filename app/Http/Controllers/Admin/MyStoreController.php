<?php

namespace App\Http\Controllers\Admin;

use App\Book;
use Auth;
use App\BookPack;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyMyStoreRequest;
use App\Http\Requests\StoreMyStoreRequest;
use App\Http\Requests\UpdateMyStoreRequest;
use App\MyStore;
use App\Order;
use App\Sended;
use App\Given;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class MyStoreController extends Controller
{   

    public function spisat (Request $r){

        $amount_to_give = $r->input("book_number");
        $sent_id = $r->input("sent_id");

        $all_given = Given::where("sended_id", $sent_id)->sum('amount');
        $all_exists = Sended::where("id", $sent_id)->get();
        $left = $all_exists['0']['amount']-(($all_given)+$amount_to_give);
        if($left >=0){
            Given::insert(["sended_id"=>$sent_id, "amount" => $amount_to_give]);
        }else{

        }
        return back();
    }  

    public function dobavit(Request $r){

        $amount_to_give = $r->input("book_number");
        $sent_id = $r->input("sent_id");

        $all_exists = Sended::where("id", $sent_id)->get();
        $new_amount = $all_exists['0']['amount']+$amount_to_give;
        
        Sended::where("id", $sent_id)->update(["amount" => $new_amount]);

        return back();
    }

    public function receive (Request $r){

        //Order::where("id", $r->input("order_id"))->update(["status"=>3]);

        $order_id = $r->input("order_id");
        $sent_id = $r->input("sent_id");
        Sended::where("id", $sent_id)->update(["status"=>2]);
        $all_sended = Sended::where("order_id", $order_id)->get();
        $is_all = 1;
        foreach ($all_sended as $sent) {
            if($sent['status']!=2){
                $is_all = 0;
                break;
            }
        }
        $all_amount = Order::where("id", $order_id)->get();
        $all_sended_sum = Sended::where("order_id", $order_id)->sum('amount');
        if($is_all==1 && $all_amount['0']['number'] <= $all_sended_sum){
            Order::where("id", $order_id)->update(['status'=>3]);
        }

        $all_orders = Order::where("store_id", Auth::user()->store['id'])->get();

        return back();//view('admin.myStores.index', ["all_orders" => $all_orders]);
    }

    public function index(Request $request)
    {
        $all_orders = Order::where("store_id", Auth::user()->store['id'])->get();
        $all_sended = array();
        foreach ($all_orders as $order) {
            $all_sended[] = Sended::where("order_id", $order['id']);
        }

        return view('admin.myStores.index', ["all_orders" => $all_orders]);
    }

    public function create()
    {
        abort_if(Gate::denies('my_store_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $books = Book::all()->pluck('book_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $packs = BookPack::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $book_numbers = Order::all()->pluck('number', 'id')->prepend(trans('global.pleaseSelect'), '');

        $statuses = Order::all()->pluck('status', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.myStores.create', compact('books', 'packs', 'book_numbers', 'statuses'));
    }

    public function store(StoreMyStoreRequest $request)
    {
        $myStore = MyStore::create($request->all());

        return redirect()->route('admin.my-stores.index');
    }

    public function edit(MyStore $myStore)
    {
        abort_if(Gate::denies('my_store_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $books = Book::all()->pluck('book_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $packs = BookPack::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $book_numbers = Order::all()->pluck('number', 'id')->prepend(trans('global.pleaseSelect'), '');

        $statuses = Order::all()->pluck('status', 'id')->prepend(trans('global.pleaseSelect'), '');

        $myStore->load('book', 'pack', 'book_number', 'status');

        return view('admin.myStores.edit', compact('books', 'packs', 'book_numbers', 'statuses', 'myStore'));
    }

    public function update(UpdateMyStoreRequest $request, MyStore $myStore)
    {
        $myStore->update($request->all());

        return redirect()->route('admin.my-stores.index');
    }

    public function show(MyStore $myStore)
    {
        abort_if(Gate::denies('my_store_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $myStore->load('book', 'pack', 'book_number', 'status');

        return view('admin.myStores.show', compact('myStore'));
    }

    public function destroy(MyStore $myStore)
    {
        abort_if(Gate::denies('my_store_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $myStore->delete();

        return back();
    }

    public function massDestroy(MassDestroyMyStoreRequest $request)
    {
        MyStore::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
