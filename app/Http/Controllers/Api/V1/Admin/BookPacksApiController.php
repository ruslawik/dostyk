<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\BookPack;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBookPackRequest;
use App\Http\Requests\UpdateBookPackRequest;
use App\Http\Resources\Admin\BookPackResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BookPacksApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('book_pack_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new BookPackResource(BookPack::all());
    }

    public function store(StoreBookPackRequest $request)
    {
        $bookPack = BookPack::create($request->all());

        return (new BookPackResource($bookPack))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(BookPack $bookPack)
    {
        abort_if(Gate::denies('book_pack_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new BookPackResource($bookPack);
    }

    public function update(UpdateBookPackRequest $request, BookPack $bookPack)
    {
        $bookPack->update($request->all());

        return (new BookPackResource($bookPack))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(BookPack $bookPack)
    {
        abort_if(Gate::denies('book_pack_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $bookPack->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
