<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMyStoreRequest;
use App\Http\Requests\UpdateMyStoreRequest;
use App\Http\Resources\Admin\MyStoreResource;
use App\MyStore;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MyStoreApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('my_store_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new MyStoreResource(MyStore::with(['book', 'pack', 'book_number', 'status'])->get());
    }

    public function store(StoreMyStoreRequest $request)
    {
        $myStore = MyStore::create($request->all());

        return (new MyStoreResource($myStore))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(MyStore $myStore)
    {
        abort_if(Gate::denies('my_store_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new MyStoreResource($myStore->load(['book', 'pack', 'book_number', 'status']));
    }

    public function update(UpdateMyStoreRequest $request, MyStore $myStore)
    {
        $myStore->update($request->all());

        return (new MyStoreResource($myStore))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(MyStore $myStore)
    {
        abort_if(Gate::denies('my_store_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $myStore->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
