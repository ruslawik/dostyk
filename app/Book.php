<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    public $table = 'books';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const LANG_SELECT = [
        'rus' => 'Рус',
        'kaz' => 'Каз',
    ];

    protected $fillable = [
        'lang',
        'grade',
        'price',
        'pack_id',
        'book_name',
        'created_at',
        'updated_at',
        'deleted_at',
        'not_show'
    ];

    const GRADE_SELECT = [
        '4'  => '4',
        '5'  => '5',
        '6'  => '6',
        '7'  => '7',
        '8'  => '8',
        '9'  => '9',
        '10' => '10',
        '11' => '11',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class, 'book_id', 'id');
    }

    public function myStores()
    {
        return $this->hasMany(MyStore::class, 'book_id', 'id');
    }

    public function pack()
    {
        return $this->belongsTo(BookPack::class, 'pack_id');
    }
}
