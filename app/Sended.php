<?php

namespace App;
use App\Order;
use App\Given;
use Illuminate\Database\Eloquent\Model;

class Sended extends Model
{
    protected $table = "sended";

    public function all_given_sum($sent_id){
    	$all_given = Given::where("sended_id", $sent_id)->sum('amount');
    	return $all_given;
    }

}
