<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Book Packs
    Route::apiResource('book-packs', 'BookPacksApiController');

    // Books
    Route::apiResource('books', 'BookApiController');

    // Stores
    Route::apiResource('stores', 'StoreApiController');

    // Orders
    Route::apiResource('orders', 'OrderApiController');

    // My Stores
    Route::apiResource('my-stores', 'MyStoreApiController');
});
