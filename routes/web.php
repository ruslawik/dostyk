<?php

Route::redirect('/', '/login');
Route::redirect('/home', '/admin');
Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::post('/make_order', 'OrderController@make_order');
    Route::post('/send_to', 'OrderController@send_to');
    Route::post('/send_to_typo', 'OrderController@send_to_typo');
    Route::post('/receive', 'MyStoreController@receive');
    Route::post('/spisat', 'MyStoreController@spisat');
    Route::post('/dobavit', 'MyStoreController@dobavit');

    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Book Packs
    Route::delete('book-packs/destroy', 'BookPacksController@massDestroy')->name('book-packs.massDestroy');
    Route::resource('book-packs', 'BookPacksController');

    // Books
    Route::delete('books/destroy', 'BookController@massDestroy')->name('books.massDestroy');
    Route::resource('books', 'BookController');

    // Stores
    Route::delete('stores/destroy', 'StoreController@massDestroy')->name('stores.massDestroy');
    Route::resource('stores', 'StoreController');

    // Orders
    Route::delete('orders/destroy', 'OrderController@massDestroy')->name('orders.massDestroy');
    Route::resource('orders', 'OrderController');

    // My Stores
    Route::delete('my-stores/destroy', 'MyStoreController@massDestroy')->name('my-stores.massDestroy');
    Route::resource('my-stores', 'MyStoreController');
});
