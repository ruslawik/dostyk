<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToOrdersTable extends Migration
{
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedInteger('store_id');

            $table->foreign('store_id', 'store_fk_465496')->references('id')->on('stores');

            $table->unsignedInteger('book_id');

            $table->foreign('book_id', 'book_fk_465497')->references('id')->on('books');

            $table->unsignedInteger('pack_id')->nullable();

            $table->foreign('pack_id', 'pack_fk_465498')->references('id')->on('book_packs');
        });
    }
}
