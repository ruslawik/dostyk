<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');

            $table->string('book_name')->nullable();

            $table->string('grade')->nullable();

            $table->string('lang')->nullable();

            $table->integer('price')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
