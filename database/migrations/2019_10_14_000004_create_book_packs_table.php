<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookPacksTable extends Migration
{
    public function up()
    {
        Schema::create('book_packs', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->nullable();

            $table->string('comment')->nullable();

            $table->datetime('date_from')->nullable();

            $table->datetime('date_to')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
