<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToMyStoresTable extends Migration
{
    public function up()
    {
        Schema::table('my_stores', function (Blueprint $table) {
            $table->unsignedInteger('book_id')->nullable();

            $table->foreign('book_id', 'book_fk_465509')->references('id')->on('books');

            $table->unsignedInteger('pack_id')->nullable();

            $table->foreign('pack_id', 'pack_fk_465510')->references('id')->on('book_packs');

            $table->unsignedInteger('book_number_id')->nullable();

            $table->foreign('book_number_id', 'book_number_fk_465511')->references('id')->on('orders');

            $table->unsignedInteger('status_id')->nullable();

            $table->foreign('status_id', 'status_fk_465512')->references('id')->on('orders');
        });
    }
}
