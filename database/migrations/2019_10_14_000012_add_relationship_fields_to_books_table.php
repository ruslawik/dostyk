<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToBooksTable extends Migration
{
    public function up()
    {
        Schema::table('books', function (Blueprint $table) {
            $table->unsignedInteger('pack_id')->nullable();

            $table->foreign('pack_id', 'pack_fk_465397')->references('id')->on('book_packs');
        });
    }
}
