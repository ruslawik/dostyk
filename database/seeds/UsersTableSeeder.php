<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => '$2y$10$7sd0gzkzwwFAD2pBbFu1Ne1g6CyJKvV3hiXzgHzJYe2wNq4SOrD82',
                'remember_token' => null,
                'phone'          => '',
                'address'        => '',
            ],
        ];

        User::insert($users);
    }
}
