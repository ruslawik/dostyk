<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => '1',
                'title' => 'user_management_access',
            ],
            [
                'id'    => '2',
                'title' => 'permission_create',
            ],
            [
                'id'    => '3',
                'title' => 'permission_edit',
            ],
            [
                'id'    => '4',
                'title' => 'permission_show',
            ],
            [
                'id'    => '5',
                'title' => 'permission_delete',
            ],
            [
                'id'    => '6',
                'title' => 'permission_access',
            ],
            [
                'id'    => '7',
                'title' => 'role_create',
            ],
            [
                'id'    => '8',
                'title' => 'role_edit',
            ],
            [
                'id'    => '9',
                'title' => 'role_show',
            ],
            [
                'id'    => '10',
                'title' => 'role_delete',
            ],
            [
                'id'    => '11',
                'title' => 'role_access',
            ],
            [
                'id'    => '12',
                'title' => 'user_create',
            ],
            [
                'id'    => '13',
                'title' => 'user_edit',
            ],
            [
                'id'    => '14',
                'title' => 'user_show',
            ],
            [
                'id'    => '15',
                'title' => 'user_delete',
            ],
            [
                'id'    => '16',
                'title' => 'user_access',
            ],
            [
                'id'    => '17',
                'title' => 'book_pack_create',
            ],
            [
                'id'    => '18',
                'title' => 'book_pack_edit',
            ],
            [
                'id'    => '19',
                'title' => 'book_pack_show',
            ],
            [
                'id'    => '20',
                'title' => 'book_pack_delete',
            ],
            [
                'id'    => '21',
                'title' => 'book_pack_access',
            ],
            [
                'id'    => '22',
                'title' => 'book_create',
            ],
            [
                'id'    => '23',
                'title' => 'book_edit',
            ],
            [
                'id'    => '24',
                'title' => 'book_show',
            ],
            [
                'id'    => '25',
                'title' => 'book_delete',
            ],
            [
                'id'    => '26',
                'title' => 'book_access',
            ],
            [
                'id'    => '27',
                'title' => 'store_create',
            ],
            [
                'id'    => '28',
                'title' => 'store_edit',
            ],
            [
                'id'    => '29',
                'title' => 'store_show',
            ],
            [
                'id'    => '30',
                'title' => 'store_delete',
            ],
            [
                'id'    => '31',
                'title' => 'store_access',
            ],
            [
                'id'    => '32',
                'title' => 'order_create',
            ],
            [
                'id'    => '33',
                'title' => 'order_edit',
            ],
            [
                'id'    => '34',
                'title' => 'order_show',
            ],
            [
                'id'    => '35',
                'title' => 'order_delete',
            ],
            [
                'id'    => '36',
                'title' => 'order_access',
            ],
            [
                'id'    => '37',
                'title' => 'my_store_create',
            ],
            [
                'id'    => '38',
                'title' => 'my_store_edit',
            ],
            [
                'id'    => '39',
                'title' => 'my_store_show',
            ],
            [
                'id'    => '40',
                'title' => 'my_store_delete',
            ],
            [
                'id'    => '41',
                'title' => 'my_store_access',
            ],
        ];

        Permission::insert($permissions);
    }
}
