<?php

return [
    'userManagement' => [
        'title'          => 'Управление пользователями',
        'title_singular' => 'Управление пользователями',
    ],
    'permission'     => [
        'title'          => 'Доступы',
        'title_singular' => 'Доступ',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'title'             => 'Title',
            'title_helper'      => '',
            'created_at'        => 'Created at',
            'created_at_helper' => '',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => '',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => '',
        ],
    ],
    'role'           => [
        'title'          => 'Роли',
        'title_singular' => 'Роль',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => '',
            'title'              => 'Title',
            'title_helper'       => '',
            'permissions'        => 'Permissions',
            'permissions_helper' => '',
            'created_at'         => 'Created at',
            'created_at_helper'  => '',
            'updated_at'         => 'Updated at',
            'updated_at_helper'  => '',
            'deleted_at'         => 'Deleted at',
            'deleted_at_helper'  => '',
        ],
    ],
    'user'           => [
        'title'          => 'Пользователи',
        'title_singular' => 'Пользователь',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'name'                     => 'ФИО',
            'name_helper'              => '',
            'email'                    => 'Email',
            'email_helper'             => '',
            'email_verified_at'        => 'Email verified at',
            'email_verified_at_helper' => '',
            'password'                 => 'Пароль',
            'password_helper'          => '',
            'roles'                    => 'Roles',
            'roles_helper'             => '',
            'remember_token'           => 'Remember Token',
            'remember_token_helper'    => '',
            'created_at'               => 'Created at',
            'created_at_helper'        => '',
            'updated_at'               => 'Updated at',
            'updated_at_helper'        => '',
            'deleted_at'               => 'Deleted at',
            'deleted_at_helper'        => '',
            'phone'                    => 'Телефон',
            'phone_helper'             => 'Телефон',
            'store'                    => 'Склад',
            'store_helper'             => 'Склад',
            'address'                  => 'Адрес доставки',
            'address_helper'           => 'Адрес доставки',
        ],
    ],
    'bookPack'       => [
        'title'          => 'Партии книг',
        'title_singular' => 'Партии книг',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'name'              => 'Название партии',
            'name_helper'       => 'Название партии',
            'comment'           => 'Комментарий',
            'comment_helper'    => 'Комментарий',
            'date_from'         => 'Период поставки от',
            'date_from_helper'  => 'Период поставки от',
            'date_to'           => 'Период поставки до',
            'date_to_helper'    => 'Период поставки до',
            'created_at'        => 'Created at',
            'created_at_helper' => '',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => '',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => '',
        ],
    ],
    'book'           => [
        'title'          => 'Книги',
        'title_singular' => 'Книги',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'book_name'         => 'Название книги',
            'book_name_helper'  => 'Название книги',
            'grade'             => 'Класс',
            'grade_helper'      => 'Класс',
            'lang'              => 'Язык обучения',
            'lang_helper'       => 'Язык обучения',
            'price'             => 'Цена',
            'price_helper'      => 'Цена',
            'created_at'        => 'Created at',
            'created_at_helper' => '',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => '',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => '',
            'pack'              => 'Партия',
            'pack_helper'       => 'Партия',
        ],
    ],
    'store'          => [
        'title'          => 'Склады',
        'title_singular' => 'Склады',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'name'              => 'Наименование обр. центра',
            'name_helper'       => 'Наименование обр. центра',
            'created_at'        => 'Created at',
            'created_at_helper' => '',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => '',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => '',
        ],
    ],
    'order'          => [
        'title'          => 'Заказы',
        'title_singular' => 'Заказы',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'store'             => 'Заказчик',
            'store_helper'      => 'Заказчик',
            'book'              => 'Книга',
            'book_helper'       => 'Книга',
            'pack'              => 'Партия',
            'pack_helper'       => 'Партия',
            'number'            => 'Количество',
            'number_helper'     => 'Количество',
            'status'            => 'Статус заказа',
            'status_helper'     => 'Статус заказа',
            'order_date'        => 'Дата заказа',
            'order_date_helper' => 'Дата заказа',
            'created_at'        => 'Created at',
            'created_at_helper' => '',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => '',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => '',
        ],
    ],
    'myStore'        => [
        'title'          => 'Мой склад',
        'title_singular' => 'Мой склад',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => '',
            'book'               => 'Наименование книги',
            'book_helper'        => 'Наименование книги',
            'pack'               => 'Партия',
            'pack_helper'        => 'Партия',
            'book_number'        => 'Количество книг',
            'book_number_helper' => 'Количество книг',
            'status'             => 'Статус заказа',
            'status_helper'      => 'Статус заказа',
            'created_at'         => 'Created at',
            'created_at_helper'  => '',
            'updated_at'         => 'Updated at',
            'updated_at_helper'  => '',
            'deleted_at'         => 'Deleted at',
            'deleted_at_helper'  => '',
        ],
    ],
];
