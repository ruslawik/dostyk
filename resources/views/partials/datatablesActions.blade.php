@can($viewGate)
    <a class="btn btn-xs btn-primary" href="{{ route('admin.' . $crudRoutePart . '.show', $row->id) }}">
        {{ trans('global.view') }}
    </a>
@endcan
@if(isset($order))
@can($order)
        <input type="text" name="book_{{$row->id}}" value="0" class="form-control">
        <!--<input type="hidden" name="book_id{{$row->id}}" value="{{$row->id}}">!-->
@endcan
@endif

@if(isset($send_typo) && $row->status=="0")
@can($send_typo)
    <form action="/admin/send_to_typo" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="order_id" value="{{$row->id}}">
        <input type="submit" class="btn btn-xs btn-success" value="Отправить в типографию">
    </form>
@endcan
@endif

@if(isset($send) && $row->status!="2")
@can($send)
    <form action="/admin/send_to" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="order_id" value="{{$row->id}}">
        <input type="text" name="book_number" value="0" class="form-control">
        <input type="submit" class="btn btn-xs btn-success" value="Отправить получателю">
    </form>
@endcan
@endif

@if(isset($receive) && $row->status!="3")
@can($receive)
    <form action="/admin/receive" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="order_id" value="{{$row->id}}">
        <input type="submit" class="btn btn-xs btn-success" value="Принять на склад">
    </form>
@endcan
@endif

@can($editGate)
    <a class="btn btn-xs btn-info" href="{{ route('admin.' . $crudRoutePart . '.edit', $row->id) }}">
        {{ trans('global.edit') }}
    </a>
@endcan
@can($deleteGate)
    <form action="{{ route('admin.' . $crudRoutePart . '.destroy', $row->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
    </form>
@endcan