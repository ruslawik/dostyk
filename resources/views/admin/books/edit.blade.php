@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.book.title_singular') }}
                </div>
                <div class="panel-body">

                    <form action="{{ route("admin.books.update", [$book->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group {{ $errors->has('book_name') ? 'has-error' : '' }}">
                            <label for="book_name">{{ trans('cruds.book.fields.book_name') }}</label>
                            <input type="text" id="book_name" name="book_name" class="form-control" value="{{ old('book_name', isset($book) ? $book->book_name : '') }}">
                            @if($errors->has('book_name'))
                                <p class="help-block">
                                    {{ $errors->first('book_name') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.book.fields.book_name_helper') }}
                            </p>
                        </div>
                        <div class="form-group {{ $errors->has('grade') ? 'has-error' : '' }}">
                            <label for="grade">{{ trans('cruds.book.fields.grade') }}</label>
                            <select id="grade" name="grade" class="form-control">
                                <option value="" disabled {{ old('grade', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                                @foreach(App\Book::GRADE_SELECT as $key => $label)
                                    <option value="{{ $key }}" {{ old('grade', $book->grade) === (string)$key ? 'selected' : '' }}>{{ $label }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('grade'))
                                <p class="help-block">
                                    {{ $errors->first('grade') }}
                                </p>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('lang') ? 'has-error' : '' }}">
                            <label for="lang">{{ trans('cruds.book.fields.lang') }}</label>
                            <select id="lang" name="lang" class="form-control">
                                <option value="" disabled {{ old('lang', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                                @foreach(App\Book::LANG_SELECT as $key => $label)
                                    <option value="{{ $key }}" {{ old('lang', $book->lang) === (string)$key ? 'selected' : '' }}>{{ $label }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('lang'))
                                <p class="help-block">
                                    {{ $errors->first('lang') }}
                                </p>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
                            <label for="price">{{ trans('cruds.book.fields.price') }}</label>
                            <input type="number" id="price" name="price" class="form-control" value="{{ old('price', isset($book) ? $book->price : '') }}" step="1">
                            @if($errors->has('price'))
                                <p class="help-block">
                                    {{ $errors->first('price') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.book.fields.price_helper') }}
                            </p>
                        </div>
                        <div class="form-group {{ $errors->has('pack_id') ? 'has-error' : '' }}">
                            <label for="pack">{{ trans('cruds.book.fields.pack') }}</label>
                            <select name="pack_id" id="pack" class="form-control select2">
                                @foreach($packs as $id => $pack)
                                    <option value="{{ $id }}" {{ (isset($book) && $book->pack ? $book->pack->id : old('pack_id')) == $id ? 'selected' : '' }}>{{ $pack }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('pack_id'))
                                <p class="help-block">
                                    {{ $errors->first('pack_id') }}
                                </p>
                            @endif
                        </div>
                        <label for="not_sh">Не показывать при заказе</label>
                            <select name="not_show" id="not_sh" class="form-control select2">
                                <?php
                                    if($book->not_show==0){
                                ?>
                                    <option value="0">Показывать</option>
                                    <option value="1">Не показывать</option>
                                <?php
                                    }else{
                                        ?>
                                        <option value="1">Не показывать</option>
                                        <option value="0">Показывать</option>
                                        <?php
                                    }
                                ?>
                            </select>
                            <br><br>
                        <div>
                            <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                        </div>
                    </form>


                </div>
            </div>

        </div>
    </div>
</div>
@endsection