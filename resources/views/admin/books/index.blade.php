@extends('layouts.admin')
@section('content')
<div class="content">
    @can('book_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.books.create") }}">
                    {{ trans('global.add') }} {{ trans('cruds.book.title_singular') }}
                </a>
            </div>
        </div>
    @endcan
    @if($errors->any())
      <h4>{{$errors->first()}}</h4>
    @endif
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('cruds.book.title_singular') }} {{ trans('global.list') }}
                </div>
                <div class="panel-body">
                  <form action="/admin/make_order" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Book">
                        <thead>
                            <tr>
                                <th width="10">
                                  Выбрать
                                </th>
                                <th>
                                    {{ trans('cruds.book.fields.id') }}
                                </th>
                                <th>
                                    {{ trans('cruds.book.fields.book_name') }}
                                </th>
                                <th>
                                    {{ trans('cruds.book.fields.grade') }}
                                </th>
                                <th>
                                    {{ trans('cruds.book.fields.lang') }}
                                </th>
                                <th>
                                    {{ trans('cruds.book.fields.price') }}
                                </th>
                                <th>
                                    {{ trans('cruds.book.fields.pack') }}
                                </th>
                                <th>
                                  <?php
                                    if(Auth::user()->roles[0]['id']==1){
                                      print("Действия");
                                    }else if(Auth::user()->roles[0]['id']==2){
                                  ?>
                                    <input type="submit" class="btn btn-success" value="Заказать"><br>
                                    (Все книги больше 0)
                                    <?php
                                    }else if(Auth::user()->roles[0]['id']==3){
                                      print("Действия");
                                    }
                                    ?>
                                </th>
                            </tr>
                        </thead>
                    </table>
                  </form>

                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('book_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.books.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
          return entry.id
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: "{{ route('admin.books.index') }}",
    columns: [
      { data: 'placeholder', name: 'placeholder' },
{ data: 'id', name: 'id' },
{ data: 'book_name', name: 'book_name' },
{ data: 'grade', name: 'grade' },
{ data: 'lang', name: 'lang' },
{ data: 'price', name: 'price' },
{ data: 'pack_name', name: 'pack.name' },
{ data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  };
  $('.datatable-Book').DataTable(dtOverrideGlobals);
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
});

</script>
@endsection