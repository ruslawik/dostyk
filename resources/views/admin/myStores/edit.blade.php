@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.myStore.title_singular') }}
                </div>
                <div class="panel-body">

                    <form action="{{ route("admin.my-stores.update", [$myStore->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group {{ $errors->has('book_id') ? 'has-error' : '' }}">
                            <label for="book">{{ trans('cruds.myStore.fields.book') }}</label>
                            <select name="book_id" id="book" class="form-control select2">
                                @foreach($books as $id => $book)
                                    <option value="{{ $id }}" {{ (isset($myStore) && $myStore->book ? $myStore->book->id : old('book_id')) == $id ? 'selected' : '' }}>{{ $book }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('book_id'))
                                <p class="help-block">
                                    {{ $errors->first('book_id') }}
                                </p>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('pack_id') ? 'has-error' : '' }}">
                            <label for="pack">{{ trans('cruds.myStore.fields.pack') }}</label>
                            <select name="pack_id" id="pack" class="form-control select2">
                                @foreach($packs as $id => $pack)
                                    <option value="{{ $id }}" {{ (isset($myStore) && $myStore->pack ? $myStore->pack->id : old('pack_id')) == $id ? 'selected' : '' }}>{{ $pack }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('pack_id'))
                                <p class="help-block">
                                    {{ $errors->first('pack_id') }}
                                </p>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('book_number_id') ? 'has-error' : '' }}">
                            <label for="book_number">{{ trans('cruds.myStore.fields.book_number') }}</label>
                            <select name="book_number_id" id="book_number" class="form-control select2">
                                @foreach($book_numbers as $id => $book_number)
                                    <option value="{{ $id }}" {{ (isset($myStore) && $myStore->book_number ? $myStore->book_number->id : old('book_number_id')) == $id ? 'selected' : '' }}>{{ $book_number }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('book_number_id'))
                                <p class="help-block">
                                    {{ $errors->first('book_number_id') }}
                                </p>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('status_id') ? 'has-error' : '' }}">
                            <label for="status">{{ trans('cruds.myStore.fields.status') }}</label>
                            <select name="status_id" id="status" class="form-control select2">
                                @foreach($statuses as $id => $status)
                                    <option value="{{ $id }}" {{ (isset($myStore) && $myStore->status ? $myStore->status->id : old('status_id')) == $id ? 'selected' : '' }}>{{ $status }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('status_id'))
                                <p class="help-block">
                                    {{ $errors->first('status_id') }}
                                </p>
                            @endif
                        </div>
                        <div>
                            <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                        </div>
                    </form>


                </div>
            </div>

        </div>
    </div>
</div>
@endsection