@extends('layouts.admin')
@section('content')
<div class="content">
    @can('order_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.orders.create") }}">
                    {{ trans('global.add') }} {{ trans('cruds.order.title_singular') }}
                </a>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    Мой склад
                </div>
                <div class="panel-body">

                    <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Order">
                        <thead>
                            <tr>
                                <th>
                                   ID Заказа
                                </th>
                                <th>
                                    {{ trans('cruds.order.fields.store') }}
                                </th>
                                <th>
                                    {{ trans('cruds.order.fields.book') }}
                                </th>
                                <th>
                                    {{ trans('cruds.order.fields.pack') }}
                                </th>
                                <th>
                                   Количество
                                </th>
                                <th>
                                    Статус отсылки
                                </th>
                                <th>
                                    Дата отсылки
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($all_orders as $order)
                            @foreach($order->sended as $sent)
                              <tr>
                                <td>
                                    {{$order->id}}
                                </td>
                                <td>
                                    {{$order->store['name']}}
                                </td>
                                <td>
                                    {{$order->book['book_name']}}
                                </td>
                                <td>
                                    {{$order->pack['name']}}
                                </td>
                                <td>
                                   Есть: {{$sent->amount - $sent->all_given_sum($sent->id)}} / Списано: {{$sent->all_given_sum($sent->id)}}
                                  <br>
                                  <?php
                                      if($sent->status != 1){
                                        ?>
                                   <form action="/admin/spisat" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="sent_id" value="{{$sent->id}}">
                                        <input type="text" name="book_number" value="0" class="form-control">
                                        <input type="submit" class="btn btn-xs btn-success" value="Выдать/Списать">
                                  </form><br>
                                   <form action="/admin/dobavit" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="sent_id" value="{{$sent->id}}">
                                        <input type="text" name="book_number" value="0" class="form-control">
                                        <input type="submit" class="btn btn-xs btn-success" value="Добавить на склад">
                                  </form>
                                  <?php
                                    }
                                  ?>

                                </td>
                                <td>
                                   <?php
                                      if($sent->status == 1){
                                        print("Отправлено получателю");
                                        ?>
                                        <br>
                                   <form action="/admin/receive" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="order_id" value="{{$order->id}}">
                                        <input type="hidden" name="sent_id" value="{{$sent->id}}">
                                        <input type="submit" class="btn btn-xs btn-success" value="Принять на склад">
                                  </form>
                                        <?php
                                      }else{
                                        print("На складе");
                                      }
                                   ?>
                                </td>
                                <td>
                                   {{$sent->created_at}}
                                </td>
                              </tr>
                            @endforeach
                          @endforeach
                        </tbody>
                    </table>


                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
  $('.datatable-Order').DataTable();
</script>
@endsection