@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('cruds.bookPack.title_singular') }}
                </div>
                <div class="panel-body">

                    <form action="{{ route("admin.book-packs.store") }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name">{{ trans('cruds.bookPack.fields.name') }}</label>
                            <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($bookPack) ? $bookPack->name : '') }}">
                            @if($errors->has('name'))
                                <p class="help-block">
                                    {{ $errors->first('name') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.bookPack.fields.name_helper') }}
                            </p>
                        </div>
                        <div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
                            <label for="comment">{{ trans('cruds.bookPack.fields.comment') }}</label>
                            <input type="text" id="comment" name="comment" class="form-control" value="{{ old('comment', isset($bookPack) ? $bookPack->comment : '') }}">
                            @if($errors->has('comment'))
                                <p class="help-block">
                                    {{ $errors->first('comment') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.bookPack.fields.comment_helper') }}
                            </p>
                        </div>
                        <div class="form-group {{ $errors->has('date_from') ? 'has-error' : '' }}">
                            <label for="date_from">{{ trans('cruds.bookPack.fields.date_from') }}</label>
                            <input type="text" id="date_from" name="date_from" class="form-control datetime" value="{{ old('date_from', isset($bookPack) ? $bookPack->date_from : '') }}">
                            @if($errors->has('date_from'))
                                <p class="help-block">
                                    {{ $errors->first('date_from') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.bookPack.fields.date_from_helper') }}
                            </p>
                        </div>
                        <div class="form-group {{ $errors->has('date_to') ? 'has-error' : '' }}">
                            <label for="date_to">{{ trans('cruds.bookPack.fields.date_to') }}</label>
                            <input type="text" id="date_to" name="date_to" class="form-control datetime" value="{{ old('date_to', isset($bookPack) ? $bookPack->date_to : '') }}">
                            @if($errors->has('date_to'))
                                <p class="help-block">
                                    {{ $errors->first('date_to') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.bookPack.fields.date_to_helper') }}
                            </p>
                        </div>
                        <div>
                            <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                        </div>
                    </form>


                </div>
            </div>

        </div>
    </div>
</div>
@endsection