@extends('layouts.admin')
@section('content')
<div class="content">
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    Склад 
                </div>
                <div class="panel-body">

                    <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Order">
                        <thead>
                            <tr>
                                <th>
                                   ID Заказа
                                </th>
                                <th>
                                    {{ trans('cruds.order.fields.store') }}
                                </th>
                                <th>
                                    {{ trans('cruds.order.fields.book') }}
                                </th>
                                <th>
                                    {{ trans('cruds.order.fields.pack') }}
                                </th>
                                <th>
                                   Количество
                                </th>
                                <th>
                                    Статус отсылки
                                </th>
                                <th>
                                    Дата отсылки
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($all_orders as $order)
                            @foreach($order->sended as $sent)
                              <tr>
                                <td>
                                    {{$order->id}}
                                </td>
                                <td>
                                    {{$order->store['name']}}
                                </td>
                                <td>
                                    {{$order->book['book_name']}}
                                </td>
                                <td>
                                    {{$order->pack['name']}}
                                </td>
                                <td>
                                   Есть: {{$sent->amount - $sent->all_given_sum($sent->id)}} / Списано: {{$sent->all_given_sum($sent->id)}}
                                  <br>

                                </td>
                                <td>
                                   <?php
                                      if($sent->status == 1){
                                        print("Отправлено получателю");
                                        ?>
                                        <?php
                                      }else{
                                        print("На складе");
                                      }
                                   ?>
                                </td>
                                <td>
                                   {{$sent->created_at}}
                                </td>
                              </tr>
                            @endforeach
                          @endforeach
                        </tbody>
                    </table>


                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
  $('.datatable-Order').DataTable();
</script>
@endsection