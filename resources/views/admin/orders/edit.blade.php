@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.order.title_singular') }}
                </div>
                <div class="panel-body">

                    <form action="{{ route("admin.orders.update", [$order->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group {{ $errors->has('store_id') ? 'has-error' : '' }}">
                            <label for="store">{{ trans('cruds.order.fields.store') }}*</label>
                            <select name="store_id" id="store" class="form-control select2" required>
                                @foreach($stores as $id => $store)
                                    <option value="{{ $id }}" {{ (isset($order) && $order->store ? $order->store->id : old('store_id')) == $id ? 'selected' : '' }}>{{ $store }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('store_id'))
                                <p class="help-block">
                                    {{ $errors->first('store_id') }}
                                </p>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('book_id') ? 'has-error' : '' }}">
                            <label for="book">{{ trans('cruds.order.fields.book') }}*</label>
                            <select name="book_id" id="book" class="form-control select2" required>
                                @foreach($books as $id => $book)
                                    <option value="{{ $id }}" {{ (isset($order) && $order->book ? $order->book->id : old('book_id')) == $id ? 'selected' : '' }}>{{ $book }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('book_id'))
                                <p class="help-block">
                                    {{ $errors->first('book_id') }}
                                </p>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('pack_id') ? 'has-error' : '' }}">
                            <label for="pack">{{ trans('cruds.order.fields.pack') }}</label>
                            <select name="pack_id" id="pack" class="form-control select2">
                                @foreach($packs as $id => $pack)
                                    <option value="{{ $id }}" {{ (isset($order) && $order->pack ? $order->pack->id : old('pack_id')) == $id ? 'selected' : '' }}>{{ $pack }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('pack_id'))
                                <p class="help-block">
                                    {{ $errors->first('pack_id') }}
                                </p>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('number') ? 'has-error' : '' }}">
                            <label for="number">{{ trans('cruds.order.fields.number') }}*</label>
                            <input type="number" id="number" name="number" class="form-control" value="{{ old('number', isset($order) ? $order->number : '') }}" step="1" required>
                            @if($errors->has('number'))
                                <p class="help-block">
                                    {{ $errors->first('number') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.order.fields.number_helper') }}
                            </p>
                        </div>
                        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                            <label for="status">{{ trans('cruds.order.fields.status') }}</label>
                            <select id="status" name="status" class="form-control">
                                <option value="" disabled {{ old('status', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                                @foreach(App\Order::STATUS_SELECT as $key => $label)
                                    <option value="{{ $key }}" {{ old('status', $order->status) === (string)$key ? 'selected' : '' }}>{{ $label }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('status'))
                                <p class="help-block">
                                    {{ $errors->first('status') }}
                                </p>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('order_date') ? 'has-error' : '' }}">
                            <label for="order_date">{{ trans('cruds.order.fields.order_date') }}*</label>
                            <input type="text" id="order_date" name="order_date" class="form-control datetime" value="{{ old('order_date', isset($order) ? $order->order_date : '') }}" required>
                            @if($errors->has('order_date'))
                                <p class="help-block">
                                    {{ $errors->first('order_date') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.order.fields.order_date_helper') }}
                            </p>
                        </div>
                        <div>
                            <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                        </div>
                    </form>


                </div>
            </div>

        </div>
    </div>
</div>
@endsection