@extends('layouts.admin')
@section('content')
<div class="content">
    @can('order_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.orders.create") }}">
                    {{ trans('global.add') }} {{ trans('cruds.order.title_singular') }}
                </a>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('cruds.order.title_singular') }} {{ trans('global.list') }}
                </div>
                <div class="panel-body">

                    <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Order">
                        <thead>
                            <tr>
                                <th width="10">
                                  Выбрать
                                </th>
                                <th>
                                    {{ trans('cruds.order.fields.id') }}
                                </th>
                                <th>
                                    {{ trans('cruds.order.fields.store') }}
                                </th>
                                <th>
                                    {{ trans('cruds.order.fields.book') }}
                                </th>
                                <th>
                                    {{ trans('cruds.order.fields.pack') }}
                                </th>
                                <th>
                                    {{ trans('cruds.order.fields.number') }}
                                </th>
                                <th>
                                    {{ trans('cruds.order.fields.status') }}
                                </th>
                                <th>
                                    {{ trans('cruds.order.fields.order_date') }}
                                </th>
                                <th>
                                    Действия
                                </th>
                            </tr>
                        </thead>
                    </table>


                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('order_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.orders.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
          return entry.id
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: "{{ route('admin.orders.index') }}",
    columns: [
      { data: 'placeholder', name: 'placeholder' },
{ data: 'id', name: 'id' },
{ data: 'store_name', name: 'store.name' },
{ data: 'book_book_name', name: 'book.book_name' },
{ data: 'pack_name', name: 'pack.name' },
{ data: 'number', name: 'number' },
{ data: 'status', name: 'status' },
{ data: 'order_date', name: 'order_date' },
{ data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  };
  $('.datatable-Order').DataTable(dtOverrideGlobals);
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
});

</script>
@endsection